# What Is Economics?

- **Economics is the study of the use of scarce resources which have
  alternative uses**
- unmet needs
- Economics is more fundamentally about **producing** the output from scarce
  resources in the first place - turning inputs into output
- Economics studies the consequences of decisions that are made about the use
  of land, labor, capital, and other resources that go into producing the
  volume of output which determines a country's standard of living
- Scarcity and "alternative uses"
- Different kinds of economies are essentially different ways of making
  decisions about the allocation of scarce resources
- Abundance of resources doesn't automatically create an abundance of goods
- Efficiency in production
  - the rate at which inputs are turned into output
  - affects the standard of living of whole societies
- It is **NOT** money, but the volume of goods and services which determines
  whether a country is poverty stricken or prosperous
- One way of understanding the consequences of economics decisions is to look
  at them in terms of the **incentives** they create, rather than simply the
  **goals** they pursue
- Consequences matter more than incentives
  - longer run repercussions
-
